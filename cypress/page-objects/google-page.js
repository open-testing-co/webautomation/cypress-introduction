export class GooglePage {
    navigate(){
        cy.visit('https://google.com')
    }

    search(textSearching){
        cy.get('.gLFyf').type(textSearching + '{enter}')
    }

    resultContains(textContains){
        cy.get('#rcnt').should('contain', textContains)
    }
}