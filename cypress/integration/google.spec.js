/// <reference types="Cypress" />

import { GooglePage } from "../page-objects/google-page"

describe('Searching in Google', function() {
    const googlePage = new GooglePage
    it('should navigate to Google testing',function(){

        expect(true).to.equal(true)
    })
    it('should navigate to Google',() =>{
        googlePage.navigate()
        googlePage.search('Henry Andres Correa Correa')
        googlePage.resultContains('Senior QA Automation')
        cy.get("[href*='linkedin.com/in']> h3").click({timeout:20000})
    })
})